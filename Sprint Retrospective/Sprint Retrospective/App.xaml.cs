﻿using Sprint_Retrospective.ViewModels;
using Sprint_Retrospective.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Sprint_Retrospective
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    /// 
    public partial class App : Application
    {
        public static MasterViewModel ViewModel { get; set; }
        public static ViewProjects ViewProjects { get; set; }
        public static ViewSprints ViewSprints { get; set; }
        public static BacklogView ViewBackLog { get; set; }
        public static StatsView statsView { get; set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            ViewModel = new MasterViewModel();
            App.ViewModel.AllProjects = new Dictionary<string, DataTypes.Project>();
            Data.ReadJSON(Data.FILENAME);
            ViewProjects = new ViewProjects();
            ViewProjects.DataContext = ViewModel;
            ViewProjects.Show();
        }
    }
}
