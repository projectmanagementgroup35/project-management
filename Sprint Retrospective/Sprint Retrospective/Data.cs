﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FireSharp.Config;
using FireSharp.Response;
using FireSharp.Interfaces;
using Sprint_Retrospective.DataTypes;
using System.Collections.ObjectModel;
using System.Windows;
using System.IO;
using Newtonsoft.Json;

namespace Sprint_Retrospective
{
    public class Data
    {

        //private static IFirebaseConfig config = new FirebaseConfig
        //{
        //    AuthSecret = "iijlJR4HEW4zonfSPJLxv3LeJNlqoGKw3EEPjPuG",
        //    BasePath = "https://sprint-project-4b014.firebaseio.com/"
        //};

        //private static IFirebaseClient client = new FireSharp.FirebaseClient(config);

        public const string FILENAME = "projects.json";
        public static void ReadJSON(string fileName)
        {
            try
            {
                // Reads all text from the file into a single string, in our case it is only one line.
                string json = File.ReadAllText(fileName);

                // Deserializes the Json into a List of users
                App.ViewModel.AllProjects = JsonConvert.DeserializeObject<Dictionary<string, Project>>(json);

                if (App.ViewModel.AllProjects == null)
                    App.ViewModel.AllProjects = new Dictionary<string, Project>();
            }
            catch (Exception)
            {
                App.ViewModel.AllProjects = new Dictionary<string, Project>();
            }
        }

        public static void WriteJSON(string fileName)
        {
            try
            {
                // Reads all text from the file into a single string, in our case it is only one line.
                string json = JsonConvert.SerializeObject(App.ViewModel.AllProjects);

                File.WriteAllText(fileName, json);

                //client.Delete("/Projects/");
                //client.Set("/Projects/", App.ViewModel.AllProjects);


            } catch (Exception ex)
            {
                MessageBox.Show("HEY WE COULD NOT SAVE DATA! " + ex.Message);
            }
        }

        // Don't touch this, it just works. :)
        public static bool GetAllProjects()
        {
            try
            {
                if (!string.IsNullOrEmpty(App.ViewModel.CurrentProject.Key))
                {
                    if (App.ViewModel.AllProjects.ContainsKey(App.ViewModel.CurrentProject.Key))
                    {
                        App.ViewModel.CurrentProject = (from proj in App.ViewModel.AllProjects
                                                        where proj.Key.Equals(App.ViewModel.CurrentProject.Key)
                                                        select proj).First();
                    }

                    if (!string.IsNullOrEmpty(App.ViewModel.CurrentSprint.Key))
                    {
                        if (App.ViewModel.CurrentProject.Value.Sprints.ContainsKey(App.ViewModel.CurrentSprint.Key))
                        {
                            App.ViewModel.CurrentSprint = (from sprint in App.ViewModel.CurrentProject.Value.Sprints
                                                           where sprint.Key.Equals(App.ViewModel.CurrentSprint.Key)
                                                           select sprint).First();
                            App.ViewSprints.sprintComboBox.SelectedIndex = App.ViewModel.CurrentSprint.Value.SprintNumber - 1;
                        }

                        if (!string.IsNullOrEmpty(App.ViewModel.CurrentStory.Key))
                        {
                            if (App.ViewModel.CurrentSprint.Value.Stories.ContainsKey(App.ViewModel.CurrentStory.Key))
                            {
                                App.ViewModel.CurrentStory = (from story in App.ViewModel.CurrentSprint.Value.Stories
                                                              where story.Key.Equals(App.ViewModel.CurrentStory.Key)
                                                              select story).First();
                            }
                        }
                    }
                    if (App.ViewModel.CurrentBackLog != null)
                    {
                        if (App.ViewModel.CurrentBackLog.Stories.ContainsKey(App.ViewModel.CurrentStory.Key))
                        {
                            App.ViewModel.CurrentStory = (from story in App.ViewModel.CurrentBackLog.Stories
                                                          where story.Key.Equals(App.ViewModel.CurrentStory.Key)
                                                          select story).First();
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void Update()
        {
            WriteJSON(FILENAME);
            ReadJSON(FILENAME);
            Data.GetAllProjects();
        }

        public static string GetGuid()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
