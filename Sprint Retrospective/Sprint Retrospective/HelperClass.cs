﻿using Sprint_Retrospective.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprint_Retrospective
{
    public class HelperClass
    {
        public static ObservableCollection<Project> ConvertDictionaryToList(Dictionary<string, Project> dictionary)
        {
            Dictionary<string, Project> AllProjectsDictionary = Data.GetAllProjects();
            ObservableCollection<Project> tempCollection = new ObservableCollection<Project>();
            foreach (string key in Data.GetAllProjects().Keys)
            {
                Project tempProject = AllProjectsDictionary[key];
                tempCollection.Add(tempProject);
            }
            return tempCollection;
        }
    }
}
