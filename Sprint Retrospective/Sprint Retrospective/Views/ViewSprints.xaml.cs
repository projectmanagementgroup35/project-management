﻿using Sprint_Retrospective.DataTypes;
using Sprint_Retrospective.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sprint_Retrospective
{
    /// <summary>
    /// Interaction logic for ViewSprints.xaml
    /// </summary>
    public partial class ViewSprints : Window
    {
        public ViewSprints()
        {
            InitializeComponent();
            if (App.ViewModel.CurrentProject.Value.Sprints != null && App.ViewModel.CurrentProject.Value.Sprints.Count == 0)
            {
                bProjectStats.IsEnabled = false;
                bCurrentSprintStats.IsEnabled = false;
                bBacklog.IsEnabled = false;
            }
            Project.PopulateSprintsStats();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (App.ViewProjects != null)
                App.ViewProjects.Show();
        }

        private void NewSprint_Click(object sender, RoutedEventArgs e)
        {
            bProjectStats.IsEnabled = true;
            bCurrentSprintStats.IsEnabled = true;
            bBacklog.IsEnabled = true;
            var temp = App.ViewModel.CurrentProject.Value.Sprints.Count();
            Sprint sprint = new Sprint() { SprintNumber = App.ViewModel.CurrentProject.Value.Sprints != null ? App.ViewModel.CurrentProject.Value.Sprints.Count() + 1 : 1 };
            App.ViewModel.CurrentProject.Value.Sprints.Add(Data.GetGuid(), sprint);
            SetSprintComboBoxAndViewModel();
            SendUnfinishedStoriesToNextSprint(sprint);
            Data.Update();
            Project.PopulateSprintsStats();
        }

        private void SelectionChanged_SprintComboBox(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox.SelectedIndex != -1)
            {
                App.ViewModel.CurrentSprint = (from sprint in App.ViewModel.CurrentProject.Value.Sprints
                                               where sprint.Value.SprintNumber.Equals(comboBox.SelectedValue)
                                               select sprint).First();
            }
        }

        private void ViewBacklog_Click(object sender, RoutedEventArgs e)
        {
            App.ViewModel.CurrentSprint = (App.ViewModel.CurrentProject.Value.Sprints.OrderByDescending(a => a.Value.SprintNumber)).First();
            App.ViewBackLog = new BacklogView();
            App.ViewBackLog.Show();
            this.Hide();
        }

        private void bCurrentSprintStats_Click(object sender, RoutedEventArgs e)
        {
            App.statsView = new StatsView(App.ViewModel.CurrentSprint.Value);
            App.statsView.Show();
            //Statistics Stats = new Statistics(App.ViewModel.CurrentSprint.Value);
            //foreach (string member in Stats.TeamMembers)
            //{
            //    MessageBox.Show(member + " " + Stats.GetTeamMemberAccuracy(member) + "%"
            //        + "\nTotal Hours Worked: " + Stats.GetTeamMemberTotalHoursWorked(member) + " Actual Estimated: " + Stats.GetTeamMemberTotalHoursEstimate(member)
            //        + "\nTotal Points Left: " + Stats.GetTeamMemberTotalPointsWorked(member) + " Actual Estimated: " + Stats.GetTeamMemberTotalPointsEstimate(member));
            //}
        }

        private void bCurrentProjectStats_Click(object sender, RoutedEventArgs e)
        {
            App.statsView = new StatsView(App.ViewModel.CurrentProject.Value);
            App.statsView.Show();
            //Statistics Stats = new Statistics(App.ViewModel.CurrentProject.Value);
            //foreach (string member in Stats.TeamMembers)
            //{
            //    MessageBox.Show(member + " " + Stats.GetTeamMemberAccuracy(member) + "%"
            //        + "\nTotal Hours Worked: " + Stats.GetTeamMemberTotalHoursWorked(member) + " Actual Estimated: " + Stats.GetTeamMemberTotalHoursEstimate(member)
            //        + "\nTotal Points Left: " + Stats.GetTeamMemberTotalPointsWorked(member) + " Actual Estimated: " + Stats.GetTeamMemberTotalPointsEstimate(member));
            //}
        }

        private void SprintComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.ViewModel.CurrentProject.Value.Sprints != null && App.ViewModel.CurrentProject.Value.Sprints.Count != 0)
            {
                SetSprintComboBoxAndViewModel();
            }
        }

        private void SetSprintComboBoxAndViewModel()
        {
            sprintComboBox.SelectedIndex = sprintComboBox.Items.Count - 1;
            App.ViewModel.CurrentSprint = (from sprint in App.ViewModel.CurrentProject.Value.Sprints
                                           where sprint.Value.SprintNumber.Equals(sprintComboBox.SelectedValue)
                                           select sprint).First();
        }

        private void SendUnfinishedStoriesToNextSprint(Sprint sprint)
        {
            if(App.ViewModel.CurrentProject.Value.Sprints.Count > 1)
            {
                Sprint oldSprint = (from sprints in App.ViewModel.CurrentProject.Value.Sprints
                                    where sprints.Value.SprintNumber == sprint.SprintNumber - 1
                                    select sprints).First().Value;

                foreach (Story story in oldSprint.Stories.Values)
                {
                    if (story.Status != Story.EStatus.Delivered)
                        sprint.Stories.Add(Data.GetGuid(), story);
                }
            }
        }
    }
}
