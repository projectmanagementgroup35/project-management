﻿using Sprint_Retrospective.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sprint_Retrospective
{
    /// <summary>
    /// Interaction logic for EditStory.xaml
    /// </summary>
    public partial class AddEditStoryView : Window
    {
        public enum Mode { Add, Edit };
        private bool boolAssign, boolEstimate, boolStatus, boolDescription, boolTimeSpent;
        private Mode modeSelected;

        public ObservableCollection<string> ComboStatus = new ObservableCollection<string>() { "Planned", "Started", "Delivered" };
        public Story story = new Story();
        public AddEditStoryView(Mode mode)
        {
            InitializeComponent();
            cbStatus.ItemsSource = ComboStatus;
            modeSelected = mode;
            if (mode == Mode.Edit)
            {
                story = App.ViewModel.CurrentStory.Value;
                cbStatus.SelectedIndex = story.Status == Story.EStatus.Planned ? 0 : story.Status == Story.EStatus.Started ? 1 : 2;
                bAddStory.Content = "Save Change";
            }

             this.DataContext = story;
        }

        private bool FieldsAreFilled()
        {
            ValidateAndSetBooleans();
            return !string.IsNullOrWhiteSpace(tbDescription.Text) && !string.IsNullOrWhiteSpace(tbAssignTo.Text) && !string.IsNullOrWhiteSpace(tbEstimate.Text) && cbStatus.SelectedIndex != -1;
        }

        private void ValidateAndSetBooleans()
        {
            boolAssign = !string.IsNullOrWhiteSpace(tbAssignTo.Text);
            try
            {
                boolEstimate = !string.IsNullOrWhiteSpace(tbEstimate.Text) && int.Parse(tbEstimate.Text) != 0;
            } catch(Exception)
            {
                boolEstimate = false;
            }
            boolTimeSpent = !string.IsNullOrWhiteSpace(tbTimeSpent.Text);
            boolStatus = cbStatus.SelectedIndex != -1;
            boolDescription = !string.IsNullOrWhiteSpace(tbDescription.Text);
        }

        private void ToggleLabelsForErrors()
        {
            lbAssign.Foreground = boolAssign ? Brushes.Black : Brushes.Red;
            lbEstimate.Foreground = boolEstimate ? Brushes.Black : Brushes.Red;
            lbStatus.Foreground = boolStatus ? Brushes.Black : Brushes.Red;
            lbDescription.Foreground = boolDescription ? Brushes.Black : Brushes.Red;
            lbTimeSpent.Foreground = boolTimeSpent ? Brushes.Black : Brushes.Red;
        }

        private void bSaveStory_Click(object sender, RoutedEventArgs e)
        {
            if (FieldsAreFilled())
            {
                ToggleLabelsForErrors();
                story.Status = cbStatus.SelectedItem as string == ComboStatus[0] ? Story.EStatus.Planned : cbStatus.SelectedItem as string == ComboStatus[1] ? Story.EStatus.Started : Story.EStatus.Delivered;

                if (modeSelected == Mode.Add)
                {
                    story.Assignees = story.Assignees.ToLower();
                    App.ViewModel.CurrentProject.Value.BackLog.Stories.Add(Data.GetGuid(), story);
                    Data.Update();
                    App.ViewModel.CurrentBackLog = App.ViewModel.CurrentProject.Value.BackLog;
                } else if (modeSelected == Mode.Edit)
                {
                    story.Assignees = story.Assignees.ToLower();
                    bool temp = false;
                    foreach(Story tempStory in App.ViewModel.CurrentSprint.Value.Stories.Values)
                    {
                        if (tempStory == story)
                            temp = true;
                    }

                    if(temp)
                    {
                        App.ViewModel.CurrentSprint.Value.Stories[App.ViewModel.CurrentStory.Key] = story;
                    }
                    else
                    {
                        App.ViewModel.CurrentProject.Value.BackLog.Stories[App.ViewModel.CurrentStory.Key] = story;
                    }
                    
                    Data.Update();
                }
                this.Close();
            }
            else
            {
                ToggleLabelsForErrors();
            }
        }
    }
}
