﻿using Sprint_Retrospective.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sprint_Retrospective.Views
{
    /// <summary>
    /// Interaction logic for AddProject.xaml
    /// </summary>
    public partial class AddProjectView : Window
    {
        public Project project;
        public AddProjectView()
        {
            project = new Project();
            DataContext = project;
            InitializeComponent();
        }

        bool teamName, teamNumber, productName, storyPoint, initialVelocity, pivitolTracker;

        private bool FieldsAreFilled()
        {
            ValidateAndSetBooleans();
            return !string.IsNullOrWhiteSpace(tbTeamName.Text)
                && !string.IsNullOrWhiteSpace(tbTeamNumber.Text)
                && !string.IsNullOrWhiteSpace(tbProductName.Text)
                && !string.IsNullOrWhiteSpace(tbStoryPoint.Text)
                && !string.IsNullOrWhiteSpace(tbInitialVelocity.Text)
                && !string.IsNullOrWhiteSpace(tbPivitolTracker.Text);
        }

        private void ValidateAndSetBooleans()
        {
            teamName = !string.IsNullOrWhiteSpace(tbTeamName.Text);
            teamNumber = !string.IsNullOrWhiteSpace(tbTeamNumber.Text);
            productName = !string.IsNullOrWhiteSpace(tbProductName.Text);
            try
            {
                storyPoint = !string.IsNullOrWhiteSpace(tbStoryPoint.Text) && !int.Parse(tbStoryPoint.Text).Equals(0);
            } catch (Exception)
            {
                storyPoint = false;
            }
            initialVelocity = !string.IsNullOrWhiteSpace(tbInitialVelocity.Text);
            pivitolTracker = !string.IsNullOrWhiteSpace(tbPivitolTracker.Text);
        }

        private void ToggleLabelsForErrors()
        {
            lbTeamName.Foreground = teamName ? Brushes.Black : Brushes.Red;
            lbTeamNumber.Foreground = teamNumber ? Brushes.Black : Brushes.Red;
            lbProductName.Foreground = productName ? Brushes.Black : Brushes.Red;
            lbStoryHours.Foreground = storyPoint ? Brushes.Black : Brushes.Red;
            lbInitialVelocity.Foreground = initialVelocity ? Brushes.Black : Brushes.Red;
            lbPivotalTracker.Foreground = pivitolTracker ? Brushes.Black : Brushes.Red;
        }

        private void SaveProject_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FieldsAreFilled())
                {
                    App.ViewModel.AllProjects.Add(Data.GetGuid(), project);
                    Data.Update();
                    this.Close();
                }
                else ToggleLabelsForErrors();
            } catch(Exception)
            {
                MessageBox.Show("Unable to add Project");
            }
        }
    }
}
