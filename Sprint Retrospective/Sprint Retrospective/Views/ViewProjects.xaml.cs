﻿using Sprint_Retrospective.DataTypes;
using Sprint_Retrospective.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sprint_Retrospective
{
    /// <summary>
    /// Interaction logic for ViewProjects.xaml
    /// </summary>
    public partial class ViewProjects : Window
    {
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            App.Current.Shutdown();
        }
        public ViewProjects()
        {
            InitializeComponent();
            if (App.ViewModel.AllProjects != null && App.ViewModel.AllProjects.Count > 0)
            {
                App.ViewModel.CurrentProject = App.ViewModel.AllProjects.First();
            }
        }

        private void OpenProject_Click(object sender, RoutedEventArgs e)
        {
            if (App.ViewModel.CurrentProject.Value != null)
            {
                if (App.ViewModel.CurrentProject.Value.Sprints != null)
                    if (App.ViewModel.CurrentProject.Value.Sprints.Count() > 0)
                        App.ViewModel.CurrentSprint = App.ViewModel.CurrentProject.Value.Sprints.First();
                    else
                        App.ViewModel.CurrentProject.Value.Sprints = new Dictionary<string, Sprint>();
                App.ViewSprints = new ViewSprints();
                App.ViewSprints.DataContext = App.ViewModel;
                App.ViewSprints.Show();
                this.Hide();
            }
        }

        private void DeleteProject_Click(object sender, RoutedEventArgs e)
        {
            if(dataGrid.SelectedItem != null)
            {
                App.ViewModel.AllProjects.Remove(App.ViewModel.CurrentProject.Key);
                Data.Update();
            }
        }

        private void AddProject_Click(object sender, RoutedEventArgs e)
        {
            AddProjectView addProjectView = new AddProjectView();
            addProjectView.ShowDialog();
        }

        private void DG_Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Hyperlink link = (Hyperlink)e.OriginalSource;
                System.Diagnostics.Process.Start(link.NavigateUri.AbsoluteUri);
            } catch (Exception)
            {
            }
        }
    }
}
