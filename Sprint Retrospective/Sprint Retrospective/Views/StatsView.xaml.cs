﻿using Sprint_Retrospective.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sprint_Retrospective.Views
{
    /// <summary>
    /// Interaction logic for StatsView.xaml
    /// </summary>
    public partial class StatsView : Window
    {
        public Statistics Statistics;

        public ObservableCollection<StatViewer> stats
        {
            get { return (ObservableCollection<StatViewer>)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("stats", typeof(ObservableCollection<StatViewer>),
                typeof(StatsView), new PropertyMetadata(null));

        public StatsView(Project project)
        {
            InitializeComponent();
            this.Title = "Stats For " + project.ProductName + " Project";
            Statistics = new Statistics(project);
            stats = new ObservableCollection<StatViewer>();
            foreach (string member in Statistics.TeamMembers)
            {
                StatViewer stat = new StatViewer(member);
                stat.Accuracy = Statistics.GetTeamMemberAccuracy(member) + "%";
                stat.HoursWorked = Statistics.GetTeamMemberTotalHoursWorked(member);
                stat.HoursEstimate = Statistics.GetTeamMemberTotalHoursEstimate(member);
                stat.PointsWorked = Statistics.GetTeamMemberTotalPointsWorked(member);
                stat.PointsEstimate = Statistics.GetTeamMemberTotalPointsEstimate(member);
                stats.Add(stat);
            }
            DataContext = this;
        }

        public StatsView(Sprint sprint)
        {
            InitializeComponent();
            this.Title = "Stats For Sprint " + sprint.SprintNumber;
            Statistics = new Statistics(sprint);
            stats = new ObservableCollection<StatViewer>();
            foreach (string member in Statistics.TeamMembers)
            {
                StatViewer stat = new StatViewer(member);
                stat.Accuracy = Statistics.GetTeamMemberAccuracy(member) + "%";
                stat.HoursWorked = Statistics.GetTeamMemberTotalHoursWorked(member);
                stat.HoursEstimate = Statistics.GetTeamMemberTotalHoursEstimate(member);
                stat.PointsWorked = Statistics.GetTeamMemberTotalPointsWorked(member);
                stat.PointsEstimate = Statistics.GetTeamMemberTotalPointsEstimate(member);
                stats.Add(stat);
            }
            DataContext = this;
        }

        public class StatViewer
        {
            public string Name { get; set; }
            public string Accuracy { get; set; }
            public double HoursWorked { get; set; }
            public double HoursEstimate { get; set; }
            public double PointsWorked { get; set; }
            public double PointsEstimate { get; set; }

            public StatViewer(string name)
            {
                Name = name;
            }

        }
    }
}
