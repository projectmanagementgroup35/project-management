﻿using Sprint_Retrospective.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Sprint_Retrospective
{
    /// <summary>
    /// Interaction logic for BacklogView.xaml
    /// </summary>
    public partial class BacklogView : Window
    {
        public BacklogView()
        {
            InitializeComponent();
            App.ViewModel.CurrentBackLog = App.ViewModel.CurrentProject.Value.BackLog;
            sprintGroupBox.Header = "Sprint " + App.ViewModel.CurrentSprint.Value.SprintNumber;
            this.DataContext = App.ViewModel;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            if (App.ViewSprints != null)
                App.ViewSprints.Show();
        }

        private void MoveStoryToBackLog_Click(object sender, RoutedEventArgs e)
        {
            var items = dgSprint.SelectedItems;
            while (items.Count > 0)
            {
                App.ViewModel.CurrentStory = (KeyValuePair<string, Story>)items[0];
                App.ViewModel.CurrentBackLog.Stories.Add(Data.GetGuid(), App.ViewModel.CurrentStory.Value);
                App.ViewModel.CurrentSprint.Value.Stories.Remove(App.ViewModel.CurrentStory.Key);
                items.RemoveAt(0);
            }
            Data.Update();
            App.ViewModel.CurrentBackLog = App.ViewModel.CurrentProject.Value.BackLog;
            Project.PopulateSprintsStats();
        }

        private void MoveStoryToSprint_Click(object sender, RoutedEventArgs e)
        {
            var items = dgBacklog.SelectedItems;
            while (items.Count > 0)
            {
                App.ViewModel.CurrentStory = (KeyValuePair<string, Story>)items[0];
                App.ViewModel.CurrentSprint.Value.Stories.Add(Data.GetGuid(), App.ViewModel.CurrentStory.Value);
                App.ViewModel.CurrentBackLog.Stories.Remove(App.ViewModel.CurrentStory.Key);
                items.RemoveAt(0);
            }
            Data.Update();
            App.ViewModel.CurrentBackLog = App.ViewModel.CurrentProject.Value.BackLog;
            Project.PopulateSprintsStats();
        }

        private void AddStory_Click(object sender, RoutedEventArgs e)
        {
            AddEditStoryView addStory = new AddEditStoryView(AddEditStoryView.Mode.Add);
            addStory.Show();
        }

        private void EditStory_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dgSprint.SelectedIndex != -1)
                {
                    App.ViewModel.CurrentStory = (from story in App.ViewModel.CurrentSprint.Value.Stories
                                                  where story.Equals(dgSprint.SelectedItem)
                                                  select story).First();
                }
                else if (dgBacklog.SelectedIndex != -1)
                {
                    App.ViewModel.CurrentStory = (from story in App.ViewModel.CurrentBackLog.Stories
                                                  where story.Equals(dgBacklog.SelectedItem)
                                                  select story).First();
                }
                else throw new Exception("Lazy man");

                AddEditStoryView editStory = new AddEditStoryView(AddEditStoryView.Mode.Edit);
                editStory.Show();
            }
            catch (Exception)
            {
            }
        }

        private bool somethingchanging = false;
        private void DGSprint_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!somethingchanging)
            {
                somethingchanging = true;
                dgBacklog.SelectedIndex = -1;
                dgBacklog.SelectedItem = null;
                somethingchanging = false;
            }
        }

        private void DGBackLog_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!somethingchanging)
            {
                somethingchanging = true;
                dgSprint.SelectedIndex = -1;
                dgSprint.SelectedItem = null;
                somethingchanging = false;
            }
        }
    }
}
