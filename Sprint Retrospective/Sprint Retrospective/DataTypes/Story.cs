﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprint_Retrospective.DataTypes
{
    public class Story
    {
        public enum EStatus { Planned, Started, Delivered};


        public Story()
        {
            Assignees = "";
            Description = "";
            TimeSpent = 0;
            Estimate = 1;
            Status = EStatus.Planned;
        }
        public string Assignees { get; set; }
        public string Description { get; set; }
        public int Estimate { get; set; }
        public double TimeSpent { get; set; }
        public EStatus Status { get; set; }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
