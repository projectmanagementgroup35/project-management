﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprint_Retrospective.DataTypes
{
    public class BackLog
    {
        public BackLog()
        {
            Stories = new Dictionary<string,Story>();
        }
        public Dictionary<string, Story> Stories { get; set; }
    }
}
