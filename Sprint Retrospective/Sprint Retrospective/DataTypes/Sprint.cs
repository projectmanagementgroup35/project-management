﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprint_Retrospective.DataTypes
{
    public class Sprint : INotifyPropertyChanged
    {
        public Sprint()
        {
            Stories = new Dictionary<string, Story>();
            RemainingEstimate = 0;
            TotalEstimate = 0;
        }
        public int SprintNumber { get; set; }
        private double remainingEstimate_;

        public double RemainingEstimate
        {
            get { return remainingEstimate_; }
            set
            {
                if (remainingEstimate_ != value)
                {
                    remainingEstimate_ = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("RemainingEstimate"));
                }
            }
        }

        private double totalEstimate_;

        public double TotalEstimate
        {
            get { return totalEstimate_; }
            set
            {
                if(totalEstimate_ != value)
                {
                    totalEstimate_ = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TotalEstimate"));
                }
            }
        }

        public Dictionary<string, Story> Stories { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
