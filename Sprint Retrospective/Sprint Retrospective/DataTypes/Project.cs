﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprint_Retrospective.DataTypes
{
    public class Project
    {
        public Project()
        {
            Sprints = new Dictionary<string,Sprint>();
            BackLog = new BackLog();
            ProductName = "";
            TeamName = "";
            TeamNumber = "";
            ProjectStartDate = DateTime.Now;
            InitialVelocity = 0;
            StoryPointHours = 1;
            // Need this to be uri for a hyper link
            PivitolTracker = "";
        }
        public string ProductName { get; set; }
        public string TeamName { get; set; }
        public string TeamNumber { get; set; }
        public DateTime ProjectStartDate { get; set; }
        public double InitialVelocity { get; set; }
        public int StoryPointHours { get; set; }
        public string PivitolTracker { get; set; }
        public Dictionary<string, Sprint> Sprints { get; set; }

        public BackLog BackLog { get; set; }

        public static void PopulateSprintsStats()
        {
            foreach (Sprint sprint in App.ViewModel.CurrentProject.Value.Sprints.Values)
            {
                sprint.RemainingEstimate = 0;
                sprint.TotalEstimate = 0;
                Statistics stats = new Statistics(sprint);
                foreach (string member in stats.TeamMembers)
                {
                    sprint.RemainingEstimate += stats.GetTeamMemberTotalPointsWorked(member);
                    sprint.TotalEstimate += stats.GetTeamMemberTotalPointsEstimate(member);
                }
            }
        }
    }
}
