﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;

namespace Sprint_Retrospective.DataTypes
{
    public class Statistics
    {
        public Statistics(Project project)
        {
            teamMembersCompletedStats_ = new Dictionary<string, Tuple<double, double>>();
            teamMembersHoursWorkedStats_ = new Dictionary<string, Tuple<double, double>>();
            teamMembersStoryPointsRemainingStats_ = new Dictionary<string, Tuple<double, double>>();
            PopulateDataProject(project);
        }

        public Statistics(Sprint sprint)
        {
            teamMembersCompletedStats_ = new Dictionary<string, Tuple<double, double>>();
            teamMembersHoursWorkedStats_ = new Dictionary<string, Tuple<double, double>>();
            teamMembersStoryPointsRemainingStats_ = new Dictionary<string, Tuple<double, double>>();
            PopulateDataSprint(sprint);
        }

        private Dictionary<string, Tuple<double, double>> teamMembersCompletedStats_;
        private Dictionary<string, Tuple<double, double>> teamMembersHoursWorkedStats_;
        private Dictionary<string, Tuple<double, double>> teamMembersStoryPointsRemainingStats_;
        public IEnumerable TeamMembers { get { return teamMembersCompletedStats_.Keys; } }
        public double GetTeamMemberAccuracy(string MemberName)
        {
            var member = teamMembersCompletedStats_[MemberName];
            if (member.Item2.Equals(0))
            {
                MessageBox.Show(String.Format("Divison by zero for ({0})", MemberName));
                return -1.0;
            }
            return Math.Round(((double)member.Item1 / member.Item2) * 100);
        }
        public double GetTeamMemberTotalHoursWorked(string MemberName) => teamMembersHoursWorkedStats_[MemberName].Item1;
        public double GetTeamMemberTotalHoursEstimate(string MemberName) => teamMembersHoursWorkedStats_[MemberName].Item2;

        public double GetTeamMemberTotalPointsWorked(string MemberName) => teamMembersStoryPointsRemainingStats_[MemberName].Item1;
        public double GetTeamMemberTotalPointsEstimate(string MemberName) => teamMembersStoryPointsRemainingStats_[MemberName].Item2;

        // HelperFunctions
        private void PopulateDataSprint(Sprint sprint)
        {
            foreach (Story story in sprint.Stories.Values)
            {
                AddStoryForCompletedStats(story);
                AddStoryForTotalHours(story);
                AddStoryForTotalPoints(story);
            }
        }

        private void PopulateDataProject(Project project)
        {
            foreach(Sprint sprint in project.Sprints.Values)
            {
                PopulateDataSprint(sprint);
            }
        }

        private void AddStoryForCompletedStats(Story story)
        {
            string member = story.Assignees;
            if (teamMembersCompletedStats_.ContainsKey(member))
            {
                teamMembersCompletedStats_[member] = new Tuple<double, double>
                /*Item 1*/(story.Status == Story.EStatus.Delivered ? story.TimeSpent + teamMembersCompletedStats_[member].Item1 : teamMembersCompletedStats_[member].Item1
                /*Item 2*/, teamMembersCompletedStats_[member].Item2 + (story.Estimate * App.ViewModel.CurrentProject.Value.StoryPointHours));
            }
            else
            {
                Tuple<double, double> p = new Tuple<double, double>(story.Status == Story.EStatus.Delivered ? story.TimeSpent : 0, story.Estimate * App.ViewModel.CurrentProject.Value.StoryPointHours);
                teamMembersCompletedStats_.Add(story.Assignees, p);
            }
        }

        private void AddStoryForTotalHours(Story story)
        {
            string member = story.Assignees;
            if(teamMembersHoursWorkedStats_.ContainsKey(member))
            {
                teamMembersHoursWorkedStats_[member] = new Tuple<double, double>
                /*Item 1*/(teamMembersHoursWorkedStats_[member].Item1 + story.TimeSpent
                /*Item 2*/,teamMembersHoursWorkedStats_[member].Item2 + (story.Estimate * App.ViewModel.CurrentProject.Value.StoryPointHours));
            } else
            {
                Tuple<double, double> p = new Tuple<double, double>(story.TimeSpent, (story.Estimate * App.ViewModel.CurrentProject.Value.StoryPointHours));
                teamMembersHoursWorkedStats_.Add(member, p);
            }
        }

        private void AddStoryForTotalPoints(Story story)
        {
            string member = story.Assignees;
            if (teamMembersStoryPointsRemainingStats_.ContainsKey(member))
            {
                teamMembersStoryPointsRemainingStats_[member] = new Tuple<double, double>
                /*Item 1*/(teamMembersStoryPointsRemainingStats_[member].Item1 + (story.Status != Story.EStatus.Delivered ? story.Estimate : 0)
                /*Item 2*/, teamMembersStoryPointsRemainingStats_[member].Item2 + story.Estimate);
            }
            else
            {
                Tuple<double, double> p = new Tuple<double, double>((story.Status != Story.EStatus.Delivered ? story.Estimate : 0), story.Estimate);
                teamMembersStoryPointsRemainingStats_.Add(member, p);
            }
        }

    }
}
