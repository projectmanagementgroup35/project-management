﻿using Sprint_Retrospective.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprint_Retrospective.ViewModels
{
    public class MasterViewModel : INotifyPropertyChanged
    {
        private Dictionary<string, Project> allprojects_;
        public Dictionary<string, Project> AllProjects
        {
            get { return allprojects_; }
            set
            {
                if (allprojects_ != value)
                {
                    allprojects_ = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("AllProjects"));
                }
            }
        }
        private KeyValuePair<string, Project> currentproject_;
        public KeyValuePair<string, Project> CurrentProject
        {
            get { return currentproject_; }
            set
            {
                if (currentproject_.Value != value.Value)
                {
                    currentproject_ = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CurrentProject"));
                }
            }
        }

        private KeyValuePair<string, Story> currentstory_;
        public KeyValuePair<string, Story> CurrentStory
        {
            get { return currentstory_; }
            set
            {
                if (currentstory_.Value != value.Value)
                {
                    currentstory_ = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CurrentStory"));
                }
            }
        }
        private KeyValuePair<string, Sprint> currentsprint_;
        public KeyValuePair<string, Sprint> CurrentSprint
        {
            get { return currentsprint_; }
            set
            {
                if (currentsprint_.Value != value.Value)
                {
                    currentsprint_ = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CurrentSprint"));
                }
            }
        }
        private BackLog currentbacklog_;
        public BackLog CurrentBackLog
        {
            get { return currentbacklog_; }
            set
            {
                if(currentbacklog_ != value)
                {
                    currentbacklog_ = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CurrentBacklog"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
